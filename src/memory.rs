use rysk_core::{ register::Register32, system::Mmu };

pub struct Memory {
    /// ROM for the firmware
    firmware: Vec<u8>,
    /// A single file that can be mapped to a section of memory
    file: Vec<u8>,
    /// RAM for system use
    memory: Vec<u8>
}
impl Memory {
    /// Create a new memory manager with `memory` bytes of main memory
    pub fn new(memory: usize) -> Self {
        Self {
            firmware: Default::default(),
            file: Default::default(),
            memory: unsafe {
                // Allocate a large chunk of memory. Due to a bug in Box::new and inlining, this currently requires unsafe to have reasonable performance
                core::slice::from_raw_parts(std::alloc::alloc_zeroed(std::alloc::Layout::from_size_align(memory, 1).expect("Unable to allocate main memory")) as _, memory)
            }.into()

        }
    }

    /// Load in the single memory-mappable file
    pub fn load_file<P: AsRef<std::path::Path>>(&mut self, path: P) -> std::io::Result<()> {
        use std::{fs::File, io::Read};
        let mut file = File::open(path.as_ref())?;
        if file.read_to_end(&mut self.file)? > Self::FILE_SIZE as _ {
            panic!("File {:?} is too big to fit in its address space of {} bytes", path.as_ref(), Self::FILE_SIZE)
        };

        Ok(())
    }

    /// Load in a firmware file
    pub fn load_firmware<P: AsRef<std::path::Path>>(&mut self, path: P) -> std::io::Result<()> {
        use std::{fs::File, io::Read};
        let mut file = File::open(path.as_ref())?;
        if file.read_to_end(&mut self.firmware)? > Self::FIRMWARE_SIZE as _ {
            panic!("Firmware file {:?} is too big to fit in its address space of {} bytes", path.as_ref(), Self::FIRMWARE_SIZE)
        };

        Ok(())
    }

    // Address ranges
    pub const FIRMWARE_START: u32 = 0x1000;
    pub const FIRMWARE_SIZE: u32 = 0xF000;
    pub const FIRMWARE_END: u32 = Self::FIRMWARE_START + Self::FIRMWARE_SIZE;

    pub const FILE_START: u32 = 0x1_0000;
    pub const FILE_SIZE: u32 = 0xF_0000;
    pub const FILE_END: u32 = Self::FILE_START + Self::FILE_SIZE;

    pub const MEMORY_START: u32 = 0x80_0000;
    pub const MEMORY_SIZE: u32 = 1024 * 1024 * 512;
    pub const MEMORY_END: u32 = Self::MEMORY_START + Self::MEMORY_SIZE;
}
impl Mmu<Register32> for Memory {
    fn get(&self, address: u32) -> u8 {
        match address {
            Self::FIRMWARE_START..=Self::FIRMWARE_END if ((address - Self::FIRMWARE_START)  as usize) < self.firmware.len() => self.firmware[(address - Self::FIRMWARE_START) as usize],
            Self::FILE_START..=Self::FILE_END if ((address - Self::FILE_START)  as usize) < self.file.len() => self.file[(address - Self::FILE_START) as usize],
            Self::MEMORY_START..=Self::MEMORY_END if ((address - Self::MEMORY_START)  as usize) < self.memory.len() => self.memory[(address - Self::MEMORY_START) as usize],
            _ => 0
        }
    }

    fn set(&mut self, address: u32, value: u8) {
        use std::io::Write;
        match address {
            0 => if value != 0 { std::process::exit(0) },
            2 => {std::io::stdout().write(&[value]).unwrap();},// print!("{}", unsafe { std::str::from_utf8_unchecked(&[value])}),
            Self::FIRMWARE_START..=Self::FIRMWARE_END if ((address - Self::FIRMWARE_START)  as usize) < self.firmware.len() => self.firmware[(address - Self::FIRMWARE_START) as usize] = value,
            Self::FILE_START..=Self::FILE_END if ((address - Self::FILE_START)  as usize) < self.file.len() => self.file[(address - Self::FILE_START) as usize] = value,
            Self::MEMORY_START..=Self::MEMORY_END if ((address - Self::MEMORY_START)  as usize) < self.memory.len() => self.memory[(address - Self::MEMORY_START) as usize] = value,
            _ => ()
        }
    }
}