#pragma once
#include "types.h"

#define ELF_IDENTIFIER_LENGTH 16
typedef struct elf_header {
    u8      identifier[ELF_IDENTIFIER_LENGTH];
    u16     type;
    u16     machine;
    u32     version;
    usize   entry;
    usize   program_header_table;
    usize   section_header_table;
    u32     flags;
    u16     header_size;
    u16     program_header_size;
    u16     program_header_count;
    u16     section_header_size;
    u16     section_header_count;
    u16     string_table_index;
} ElfHeader;

typedef struct elf_section_header {
    u32     name;
    u32     type;
    u32     flags;
    usize   address;
    usize   offset;
    u32     size;
    u32     link;
    u32     info;
    u32     address_align;
    u32     entry_size;
} ElfSectionHeader;

#define ELF_VERSION_INVALID 0
#define ELF_VERSION 1

#define ELF_CLASS_INVALID 0
#define ELF_CLASS_32BIT 1
#define ELF_CLASS_64BIT 2

#define ELF_LAYOUT_INVALID 0
#define ELF_LAYOUT_LSB 1
#define ELF_LAYOUT_MSB 2