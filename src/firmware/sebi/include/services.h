#pragma once
#include "types.h"
#include "sebi.h"
#include "variable.h"
#include "file.h"

const struct sebi_services sebi_services;

#define DECLARE(name, ret, params...)\
    typedef ret (*sebi_fn_ ## name)(params);\
    ret sebi_service_ ## name (params);
#define FN(name)\
    sebi_fn_ ## name name;


DECLARE(shutdown, void)
DECLARE(log, void, char* output, usize length)

DECLARE(get_variable, void, SebiVariable variable, u8* data, usize length)
DECLARE(set_variable, void, SebiVariable variable, u8* data, usize length)

// Get file information
DECLARE(get_file, struct sebi_file, guid* disk, const char* path)
// Copy a file to memory for the maximum size of file_length
// A file length of 0 means read the whole file
DECLARE(copy_file, bool, guid* disk, const char* path, usize path_length, u8* file, usize* file_length)
// Returns a function pointer to the entry point
DECLARE(load_elf, void*, u8* file, usize file_length)

typedef struct sebi_services {
    FN(shutdown)
    FN(log)

    FN(get_variable)
    FN(set_variable)

    FN(get_file)
    FN(copy_file)
    FN(load_elf)
} SebiServices;