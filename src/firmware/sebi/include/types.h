#pragma once
typedef unsigned long long u64;
typedef signed long long i64;
typedef unsigned long usize;
typedef signed long isize;
typedef unsigned int u32;
typedef signed int i32;
typedef unsigned short u16;
typedef signed short i16;
typedef unsigned char u8;
typedef signed char i8;

typedef unsigned char bool;
#define false 0
#define true 1

typedef u8 guid[16];

#define LSTR(string) string, sizeof(string)-1