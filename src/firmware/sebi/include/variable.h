#pragma once
#include "types.h"

typedef enum sebi_variable {
    SebiBootDevice,
    SebiBootPath,
    SebiBootVolume
} SebiVariable;

typedef struct sebi_variable_location {
    enum sebi_variable variable;
    usize offset;
    usize length;
} SebiVariableLocation;

typedef struct sebi_nvram {
    struct sebi_variable_location* locations;
} SebiNvram;