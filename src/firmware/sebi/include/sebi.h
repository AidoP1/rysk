#pragma once
#include "types.h"
#include "services.h"

typedef enum sebi_memory_type {
    SebiMemoryReadOnly,
    SebiMemoryWriteOnly,
    SebiMemoryReadWrite,
    // Only the end address is considered as the stack pointer defines the start address
    SebiMemoryStack,
    // Memory is in use by the sebi firmware and must not be written to
    SebiReserved,
} SebiMemoryType;

typedef struct sebi_memory_map {
    usize start_address;
    usize end_address;
    enum sebi_memory_type type;
} SebiMemoryMap;

// An interface provides information and/or procedures for an unknown protocol or specification
typedef struct sebi_interface {
    void* data;
    u16 version;
    u8 identifier[8];
} SebiInterface;

typedef struct sebi_systab {
    const struct sebi_services* services;
    const struct sebi_interface* interfaces;
    usize interfaces_length;
    struct sebi_memory_map* memory_maps;
    usize memory_maps_length;
    u16 version;
} SebiSystab;

typedef bool (*sebi_entry)(struct sebi_systab* system_table);