#include "uefi.h"
// Initialise the platform for UEFI operation
__attribute__((naked,noreturn)) void _init_platform(void) {
    asm(
        "li sp,0x900000\n"
        "call unimplemented\n"
        "li a0,1\n"
        "sb a0,(zero)"
    );
}

void println(const char* string) {
    for (; *string != 0; string++)
        *((volatile char*)2) = *string;
    *((volatile char*)2) = '\n';
    
}

__attribute__((noreturn)) void unimplemented(u32 _, ...) {
    println("\nUnimplemented UEFI function was called");
    *((volatile u8*)0) = 1;
    __builtin_unreachable();
}

static struct efi_system_table EFI_SYSTEM_TABLE = {
    .header = {
        .signature = EFI_SYSTEM_TABLE_SIGNATURE,
        .version = EFI_SPECIFICATION_VERSION,
        .header_size = sizeof(struct efi_system_table),
        .crc = 0
    },
};