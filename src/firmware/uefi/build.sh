#!/bin/sh
if [ -z "$TARGET" ]
    then TARGET="riscv32"
fi

OUT_DIR="../../../target/firmware"
OUTPUT="$OUT_DIR/uefi"
SOURCE="src/uefi.c"

mkdir -p "$OUT_DIR"
clang --target="$TARGET" -T link.ld -Og -iquote "./" "$SOURCE" -o "$OUTPUT" -nostartfiles -nodefaultlibs -mno-relax -march=rv32i -Wno-incompatible-library-redeclaration
#objcopy -I elf32-little -O binary "$OUTPUT" "$OUTPUT"