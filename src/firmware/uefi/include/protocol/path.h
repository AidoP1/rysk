#pragma once
#include "types.h"

struct efi_device_path_protocol {
    u8 type;
    u8 subtype;
    u8 length[2];
};