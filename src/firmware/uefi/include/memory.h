#pragma once
#include "types.h"

struct efi_memory_descriptor {
    u32 type;
    efi_physical_address physical_start;
    efi_virtual_address virtual_start;
    u64 pages;
    u64 attribute;
};
