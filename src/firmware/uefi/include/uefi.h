#include "types.h"

#include "table.h"
#include "boot_services.h"
#include "runtime_services.h"
#include "protocol/console.h"
#include "protocol/media.h"

#define EFI_SPECIFICATION_VERSION ((2 << 16) | (80))

#define EFI_SYSTEM_TABLE_SIGNATURE 0x5453595320494249
struct efi_system_table {
    struct efi_table_header header;
    u16* firmware_vendor;
    u32 firmware_version;

    void* stdin_handle;
    struct efi_simple_text_input_protocol* stdin_protocol;
    void* stdout_handle;
    struct efi_simple_text_output_protocol* stdout_protocol;
    void* stderr_handle;
    struct efi_simple_text_output_protocol* stderr_protocol;
    struct efi_runtime_services_table* runtime_services;
    struct efi_boot_services_table* boot_services;
    usize table_entries;
    struct efi_configuration_table* configuration_table;
};