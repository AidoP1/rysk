#pragma once
#include "types.h"
#include "table.h"
#include "memory.h"

struct efi_runtime_services_table;

FN_DEF(efi_status, get_variable, const u16* variable_name, const efi_guid* vendor_guid, u32* attributes, usize* data_size, void* data)
FN_DEF(efi_status, get_next_variable_name, usize* variable_name_size, u16* variable_name, efi_guid* guid)
FN_DEF(efi_status, set_variable, const u16* variable_name, const efi_guid* vendor_guid, const u32 attributes, const usize data_size, const void* data)
FN_DEF(efi_status, query_variable_info, const u32 attributes, u64* maximum_variable_storage_size, u64* remaining_variable_storage_size, u64* maximum_variable_size)

struct efi_time {
    u16 year;
    u8 month;
    u8 day;
    u8 hour;
    u8 minute;
    u8 second;
    u8 _pad;
    u32 nanosecond;
    i16 timezone;
    u8 daylight;
    u8 __pad;
};
struct efi_time_capabilities {
    u32 resolution;
    u32 accuracy;
    bool sets_to_zero;
};

FN_DEF(efi_status, get_time, struct efi_time* time, struct efi_time_capabilities* capabilities)
FN_DEF(efi_status, set_time, const struct efi_time* time)
FN_DEF(efi_status, get_wakeup_time, bool* enabled, bool* pending, struct efi_time* time)
FN_DEF(efi_status, set_wakeup_time, const bool enable, const struct efi_time* time)


FN_DEF(efi_status, set_virtual_address_map, const usize memory_map_size, const usize descriptor_size, const u32 descriptor_version, const struct efi_memory_descriptor* virtual_memory_map)
FN_DEF(efi_status, convert_pointer, const usize debug_disposition, void** address)

enum efi_reset_type {
    EfiResetCold,
    EfiResetWarm,
    EfiResetShutdown,
    EfiResetPlatformSpecific
};
struct efi_capsule_header {
    efi_guid capsule_guid;
    u32 header_size;
    u32 flags;
    u32 capsule_image_size;
};

FN_DEF(void, reset_system, const enum efi_reset_type, const efi_status status, const usize data_size, const void* reset_data)
FN_DEF(efi_status, get_next_high_monotonic_count, u32* high_count)
FN_DEF(efi_status, update_capsule, const struct efi_capsule_header** capsule_header_array, const usize capsule_count, efi_physical_address scatter_gather_list)
FN_DEF(efi_status, query_capsule_capabilities, const struct efi_capsule_header** capusle_header_array, const usize capsule_count, u64* maximum_capusle_size, enum efi_reset_type* reset_type)


struct efi_runtime_services_table {
    struct efi_table_header header;
    FN_PTR(get_time)
    FN_PTR(set_time)
    FN_PTR(get_wakeup_time)
    FN_PTR(set_wakeup_time)

    FN_PTR(set_virtual_address_map)
    FN_PTR(convert_pointer)

    FN_PTR(get_variable)
    FN_PTR(get_next_variable_name)
    FN_PTR(set_variable)

    FN_PTR(get_next_high_monotonic_count)
    FN_PTR(reset_system)

    FN_PTR(update_capsule)
    FN_PTR(query_capsule_capabilities)

    FN_PTR(query_variable_info)
};