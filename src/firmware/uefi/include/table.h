#pragma once
#include "types.h"

struct efi_table_header {
    u64 signature;
    u32 version;
    u32 header_size;
    u32 crc;
    u32 _reserved;
};