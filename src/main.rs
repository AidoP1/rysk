use rysk_core::{ register::{ Register32, Xlen }, system::Core };

mod memory;

fn main() {
    use memory::Memory;
    let mut mmu = Memory::new(1024*1024* 512);
    let mut core = Core::<Register32>::new(Memory::FIRMWARE_START as u32, 0);

    let mut loader = "loader.bin".to_owned();
    let mut program = None;
    let mut args = std::env::args();
    args.next().expect("Path of the executable was not the first argument");
    while let Some(arg) = args.next() {
        match arg.as_str() {
            "-l" => loader = args.next().expect("Option `-l` requires a path to the loader"),
            _ => program = Some(arg)
        }
    }

    mmu.load_firmware(loader.clone()).unwrap_or_else(|err| panic!("Unable to open loader from `{}`\n{:?}", loader, err));
    if let Some(program) = program {
        mmu.load_file(program.clone()).unwrap_or_else(|err| panic!("Unable to open program from `{}`\n{:?}", program, err));
    }

    loop {
        //let mut line = String::new();
        //println!("About to exec {:#X}\tsp = {:#x}\ta1 = {:#x}", core.pc.unsigned(), core.get(2).unsigned(), core.get(11).unsigned());
        //std::io::stdin().read_line(&mut line).unwrap();
        //println!("PC: {:#X}, SP: {:#X}", u32::from_le_bytes(*mmu.pc), u32::from_le_bytes(*mmu.registers[2]));
        core.execute(&mut mmu).unwrap_or_else(|decode_err| panic!("Decode error: {:?}\nPC at {:#X}", decode_err, core.pc.unsigned()));
    }
}
