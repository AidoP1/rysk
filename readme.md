# Rysk
A RISCV virtual machine aimed at making systems programming more accessible.

## [Rysk Core](https://gitlab.com/AidoP1/rysk-core)
The core infrastructure such as instruction decoding and execution is done in an external crate. If you need to implement your own virtual RISCV machine, you can use [Rysk Core](https://gitlab.com/AidoP1/rysk-core) to lighten the load.

## Compliance
See [Rysk Core](https://gitlab.com/AidoP1/rysk-core) for RISCV compliance.

## Goals
- [x] Firmware interface which allows assembly-free programming
- [ ] Implementation of all standard RISCV extensions
- [ ] Simple "hardware" and protocols
- [ ] Powerful debugging and tools for visualising program flow
- [ ] Multi-core support or simulation

**Future Goals**
- [ ] Graphics capabilities
- [ ] Expose host system functionality; Maybe forward through syscalls on Linux?

**Non-goals**
- Realistic virtualised hardware